from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from django.contrib.auth.models import User
from django.urls import reverse

class RandomDataAPITestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='password123')
        self.client.force_login(self.user)

    def test_get_random_data_authenticated(self):
        url = reverse('random_data', kwargs={'category': 'pokemon'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_random_data_unauthenticated(self):
        url = reverse('random_data', kwargs={'category': 'pokemon'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class LikeDislikeViewTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='password123')
        self.client.force_login(self.user)

    def test_post_like_dislike_valid_data(self):
        url = reverse('like_dislike')
        data = {'category': 1, 'character_id': 1, 'interaction_type': 'like'}
        response = self.client.post(url, data)
        response.render()
        print(response.content)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_like_dislike_invalid_data(self):
        url = reverse('like_dislike')
        # Proporcionar datos inválidos para provocar un error de validación
        data = {'category': 'invalid', 'character_id': 'invalid', 'interaction_type': 'invalid'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
