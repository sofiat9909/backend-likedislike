from rest_framework import serializers
from .models import Category, Interaction

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class InteractionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interaction
        fields = '__all__'
        
class LikeDislikeSerializer(serializers.Serializer):
    category = serializers.UUIDField()
    character_id = serializers.CharField()
    interaction_type = serializers.ChoiceField(choices=['like', 'dislike'])

    def validate_category(self, value):
        try:
            category = Category.objects.get(id=value)
        except Category.DoesNotExist:
            raise serializers.ValidationError("La categoría no existe")
        return value