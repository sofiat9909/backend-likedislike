from characters.models import Category

def insert_default_groups():
    categories = [
        {'name': 'rickandmorty', 'url': 'https://rickandmortyapi.com/api/character/'},
        {'name': 'pokemon', 'url': 'https://pokeapi.co/api/v2/pokemon/'},
        {'name': 'superhero', 'url': 'https://superheroapi.com/'}
    ]
    
    for category in categories:
        Category.objects.create(
            name=category['name'],
            url_endpoint=category['url']
        )
