from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from rest_framework.views import APIView
from rest_framework import status
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .adapters import RickAndMortyAdapter, PokeAPIAdapter, SuperheroAPIAdapter
from .models import Category, Interaction, User
from .serializers import CategorySerializer, InteractionSerializer,LikeDislikeSerializer


class RandomDataAPIView(APIView):

    def get(self, request, category):
        try:
            category_object = Category.objects.get(name=category)
        except Category.DoesNotExist:
            return Response({'error': 'Categoría no válida'}, status=400)

        category_url = category_object.url_endpoint
        
        user = request.user
        
        # Obtener interacciones previas del usuario para esta categoría
        user_interactions = Interaction.objects.filter(user=user, category=category_object)

        # Obtener IDs de personajes utilizados por el usuario
        used_character_ids = set(interaction.character_id for interaction in user_interactions)

        try:
            if category == 'rickandmorty':
                data = RickAndMortyAdapter.get_random_character(category_url, used_character_ids)
            elif category == 'pokemon':
                data = PokeAPIAdapter.get_random_pokemon(category_url, used_character_ids)
            elif category == 'superhero':
                data = SuperheroAPIAdapter.get_random_superhero(category_url, used_character_ids)
            else:
                return Response({'error': 'Categoría no válida'}, status=400)
        except ValueError as e:
            return Response({'error': str(e)}, status=400)
        
        return Response(data)
    

class CategoryListView(APIView):

    def get(self, request):
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data)


class LikeDislikeView(APIView):

    def post(self, request):
        user = request.user

        # Validar los datos recibidos utilizando el serializer
        serializer = LikeDislikeSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        # Obtener datos validados del serializer
        category_id = serializer.validated_data.get('category')
        character_id = serializer.validated_data.get('character_id')
        interaction_type = serializer.validated_data.get('interaction_type')

        # Crear la interacción
        try:
            interaction = Interaction.objects.create(
                user=user,
                category_id=category_id,
                character_id=character_id,
                interaction_type=interaction_type
            )
            serializer = InteractionSerializer(interaction)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)