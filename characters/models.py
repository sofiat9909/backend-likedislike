from django.db import models
from django.contrib.auth.models import User
import uuid

class Category(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100, unique=True)
    url_endpoint = models.URLField()

    def __str__(self):
        return self.name

class Interaction(models.Model):
    LIKE = 'like'
    DISLIKE = 'dislike'
    INTERACTION_CHOICES = [
        (LIKE, 'Like'),
        (DISLIKE, 'Dislike'),
    ]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)  # Asocia la interacción con la categoría
    character_id = models.CharField(max_length=100)  # Campo para almacenar el ID del personaje
    interaction_type = models.CharField(max_length=10, choices=INTERACTION_CHOICES)

    class Meta:
        unique_together = ['user', 'character_id']  # Un usuario no puede dar más de un like o dislike al mismo personaje

    def __str__(self):
        return f"{self.user.username} - {self.category.name} - {self.character_id} - {self.interaction_type}"