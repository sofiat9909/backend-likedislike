# urls.py
from django.urls import path
from .views import RandomDataAPIView, CategoryListView, LikeDislikeView

urlpatterns = [
    path('category/<str:category>/', RandomDataAPIView.as_view(), name='random_data'),
    path('categories/', CategoryListView.as_view(), name='category-list'),
    path('like_dislike/', LikeDislikeView.as_view(), name='like_dislike'),
]
