# adaptadores.py
import os
import requests
import random
from django.shortcuts import render
from django.http import JsonResponse
from bs4 import BeautifulSoup


class RickAndMortyAdapter:
    @classmethod
    def get_random_character(cls, url, used_character_ids):
        try:
            response = requests.get(url)

            characters = response.json()['results']

            # Filtrar los personajes aleatorios para asegurarse de que no estén en used_character_ids
            available_characters = [character for character in characters if str(character['id']) not in used_character_ids]

            if not available_characters:
                raise ValueError("No available characters")

            random_character = random.choice(available_characters)
            return random_character
        except requests.RequestException as e:
            # Manejar errores de solicitud
            raise ValueError(f"Failed to fetch random character: {e}")
        except KeyError:
            # Manejar errores de JSON si la clave 'results' no está presente en la respuesta
            raise ValueError("Failed to parse JSON response: 'results' key not found")
        except IndexError:
            # Manejar errores si la lista de personajes está vacía
            raise ValueError("No characters found in the response")
        except Exception as e:
            # Manejar cualquier otro error inesperado
            raise ValueError(f"An unexpected error occurred: {e}")
        

class PokeAPIAdapter:
    @classmethod
    def get_random_pokemon(cls, url, used_character_ids):
        try:
            response = requests.get(url)
            response.raise_for_status()  # Lanza una excepción si hay un error en la solicitud

            pokemons = response.json()['results']
            if not pokemons:
                raise ValueError("No pokemons found in the response")
            # Filtrar los Pokémon disponibles para asegurarse de que no estén en used_ids
            available_pokemons = [pokemon for pokemon in pokemons if str(pokemon['url'].split('/')[-2]) not in used_character_ids]

            if not available_pokemons:
                raise ValueError("No available pokemons")

            random_pokemon_data = random.choice(available_pokemons)
            pokemon_url = random_pokemon_data.get('url')
            if not pokemon_url:
                raise ValueError("URL of random pokemon not found")

            pokemon_details_response = requests.get(pokemon_url)
            pokemon_details_response.raise_for_status()  # Lanza una excepción si hay un error en la solicitud

            pokemon_details = pokemon_details_response.json()
            return pokemon_details
        except requests.RequestException as e:
            # Manejar errores de solicitud
            raise ValueError(f"Failed to fetch data from PokeAPI: {e}")
        except KeyError as e:
            # Manejar errores de JSON si falta una clave requerida
            raise ValueError(f"Failed to parse JSON response: {e}")
        except IndexError:
            # Manejar errores si la lista de pokemons está vacía
            raise ValueError("No pokemons found in the response")
        except Exception as e:
            # Manejar cualquier otro error inesperado
            raise ValueError(f"An unexpected error occurred: {e}")


class SuperheroAPIAdapter:
    @classmethod
    def get_random_superhero(cls, url, used_character_ids):
        token = os.environ.get('SUPERHERO_API_TOKEN')
        if not token:
            raise ValueError("Superhero API token not found")

        # Obtener el número total de superhéroes disponibles en la API
        total_superheroes = cls.get_total_superheroes(url)

        # Obtener una lista de IDs de superhéroes que el usuario ya ha calificado
        used_ids = set(used_character_ids)

        # Verificar si todos los IDs están en la lista de IDs utilizados
        if len(used_ids) == total_superheroes:
            raise ValueError("Todos los superhéroes ya han sido calificados")

        while True:
            # Generar un ID de superhéroe aleatorio dentro del rango válido
            random_id = random.randint(1, total_superheroes)

            # Verificar si el ID del superhéroe ya ha sido usado
            if random_id not in used_ids:
                break

        url = f"{url}/api.php/{token}/{random_id}"
        response = requests.get(url)
        
        if response.status_code == 200:
            superhero = response.json()
            return superhero
        else:
            raise ValueError(f"Failed to fetch superhero. Status code: {response.status_code}")

    @classmethod
    def get_total_superheroes(cls, url):
        try:
            page_url = f'{url}ids.html'
            response = requests.get(page_url)
            response.raise_for_status()  # Lanza una excepción si hay un error en la solicitud

            # Extraer los enlaces que contienen los IDs de superhéroes
            links = cls._extract_superhero_links(response.content)

            # Contar el número de enlaces encontrados
            total_superheroes = len(links)
            return total_superheroes
        except requests.RequestException as e:
            # Manejar errores de solicitud
            raise ValueError(f"Failed to fetch total superheroes: {e}")
        except Exception as e:
            # Manejar cualquier otro error
            raise ValueError(f"An error occurred: {e}")

    @staticmethod
    def _extract_superhero_links(html_content):
        # Utilizar BeautifulSoup para analizar el contenido HTML
        soup = BeautifulSoup(html_content, 'html.parser')

        # Encontrar todos los elementos <td> que contienen IDs de superhéroes
        links = soup.find_all('td', text=lambda text: text.isdigit())

        return links