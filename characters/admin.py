from django.contrib import admin
from .models import Category, Interaction


admin.site.register(Category)
admin.site.register(Interaction)
