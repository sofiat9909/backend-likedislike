from django.contrib.auth.models import AbstractUser
from .serializers import CustomUserAuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken
from django.db import models

class CustomUser(AbstractUser):
    # Define tus campos personalizados aquí
    
    # Cambia el related_name para evitar conflictos con los modelos de permisos y grupos de Django
    groups = models.ManyToManyField(
        'auth.Group',
        related_name='custom_user_groups',
        blank=True,
        verbose_name='groups',
        help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.',
    )
    user_permissions = models.ManyToManyField(
        'auth.Permission',
        related_name='custom_user_permissions',
        blank=True,
        verbose_name='user permissions',
        help_text='Specific permissions for this user.',
        related_query_name='user',
    )
class CustomAuthToken(ObtainAuthToken):
    serializer_class = CustomUserAuthTokenSerializer