from rest_framework import serializers
from django.contrib.auth import authenticate

class CustomUserAuthTokenSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(style={'input_type': 'password'}, trim_whitespace=False)

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            user = authenticate(request=self.context.get('request'), email=email, password=password)

            if not user:
                msg = 'No se encontró un usuario con estas credenciales.'
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = 'Debes proporcionar tanto el email como la contraseña.'
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs
