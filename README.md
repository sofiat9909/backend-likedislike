## Project Description:

This project is a simple backend for a "like" and "dislike" system created with Django and MongoDB, utilizing Docker for environment configuration and execution.

## Technologies Used:

- Django (Python web framework)
- MongoDB (NoSQL database)
- Docker (container platform)
- Python 3.x

## Requirements:

- Docker installed and configured
- Python 3.8 or higher
- pipenv

## Installation:

1. Clone the repository:
    ```shell
    git clone https://gitlab.com/sofiat9909/backend-likedislike.git
    ```

2. Change to the project directory:
    ```shell
    cd backend-likedislike
    ```

3. Build Docker images:
    ```shell
    docker-compose build
    ```

4. Start the containers:
    ```shell
    docker-compose up -d
    ```

## Usage:

The application has four main endpoints:

- `auth/api/token/`: Login and obtain user token
- `random-character/categories/`: Get available character categories.
- `random-character/category/{category}/`: Get a random character from the category.
- `/random-character/like_dislike/`: Register a like or dislike for a character.

### Usage Example:

Documentation [postman](https://documenter.getpostman.com/view/11107099/2sA2r3ZR6x)

## Accessing the Application:

The application will be available at the following URL:

http://localhost:8000

## Additional Resources:

- [Django](https://www.djangoproject.com/)
- [MongoDB](https://www.mongodb.com/)
- [Docker](https://www.docker.com/)

## Additional Improvements:

- Enhance security by implementing measures such as protection against SQL injection, XSS, and CSRF attacks.
- Improve interactive API documentation using tools like Swagger or Postman.
- Refactor the code to follow clean design principles and maintain an organized and easy-to-maintain codebase.
- Implement role and permission management to control category insertion.
- Improve error and exception handling to provide more descriptive and helpful error messages to users and developers.
